import csv

prev_date = ''
image_template ='<article class="panel panel-default panel-outline">\n'\
                '   <div class="panel-heading icon"> <i class="glyphicon glyphicon-picture"></i> </div>\n'\
                '   <div class="panel-body"> <img class="img-responsive img-rounded" src="./images/#NUMBER#.png" /> </div>\n'\
                '</article>\n\n'
date_template = '<div class="separator text"> <time><strong>#DATE#</strong></time><a name="#HREF#"></a> </div>\n\n'
text_template = '<article class="panel panel-info panel-outline">\n' \
                '   <div class="panel-heading icon"> <i class="glyphicon glyphicon-info-sign"></i> </div>\n' \
                '   <div class="panel-body">#TEXT#</div>\n' \
                '</article>\n\n'
output = ''
key_list = list()
date_list = list()

with open('gone_girl_fabel.csv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=';')
    line_count = 0
    for row in csv_reader:
        if line_count == 0:
            print('begin')
        else:
            # add new date banner
            if (row[1] != prev_date):
                prev_date = row[1]
                new_item = date_template.replace('#DATE#', row[1], 1)
                new_item = new_item.replace('#HREF#', 'link' + row[0],1)
                date_list.append(row[1])
                key_list.append('link' + row[0])
                output = output + new_item
            new_item = image_template.replace('#NUMBER#',row[0],1)
            output = output + new_item

            new_item = text_template.replace('#TEXT#', row[2], 1)
            output = output + new_item
            output = output + '\n<a href="#home">up!</a>\n\n'
        line_count = line_count + 1

text_file = open("tmp.html", "w")
text_file.write(output)
text_file.close()

index = '<ul class="list-unstyled">\n'
for i in range(len(date_list)):
    index = index + '<li><a href="#' + key_list[i] + '">' + date_list[i] + '</a></li>\n'
index = index + '</ul>\n'
text_file = open("overview.html", "w")
text_file.write(index)
text_file.close()